# Prueba Angular

Repositorio para mostrar prueba de desarrollo en Angular


## Versiones usadas

* **Angular-CLI** - *9.1.12*
* **Node** - *10.19.0*

## Instatalción

Descargar o clonar repositorio y ejecutar:

```
npm install
```

## Ejecución

```
npm start
```

## Autor

* **Ana Luisa Sánchez**


## Funcionalidades incluidas

-> Mostrar todos los clientes.
-> Mostrar cliente por id.
-> Crear / actualizar cliente.
-> Mostrar todos los grupos.
-> Mostrar grupo por id.
-> Mostrar miembros por id de grupo.