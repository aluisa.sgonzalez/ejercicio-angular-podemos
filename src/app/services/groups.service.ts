// Author luisa.sanchez
// Version 1.0.0

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';

// url comun
const URL = '/podemos';

@Injectable()
export class GruposService {


  constructor(private readonly http: HttpClient) { }

    /** Proposito: Enviar solicitus http para recuperar total de grupos
    * @author: luisa.sanchez
    * @version 1.0.0
   */
 obtenerGrupos():Observable<any>{

    var body = new HttpParams();
    var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.get<any>(
        URL + '/grupos',
      {
        headers, observe: 'response', params: body
      }
    ).pipe(map(res => {
        return res;
      }
      ));
    

  }

  /** Proposito: Enviar solicitus http para recuperar grupo por medio de id
    * @author: luisa.sanchez
    * @param idGrupo identificador del grupo a buscar
    * @version 1.0.0
   */
  obtenerGruposPorId(idGrupo):Observable<any>{

    
    var body = new HttpParams();
    var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.get<any>(
        URL + '/grupos/' + idGrupo,
      {
        headers, observe: 'response', params: body
      }
    ).pipe(map(res => {
        return res;
      }
      ));
    

  }

    /** Proposito: Enviar solicitus http para recuperar grupo por medio de id
    * @author: luisa.sanchez
    * @param idGrupo identificador del grupo a buscar
    * @version 1.0.0
   */
  obtenerMiembrosPorIdGrupo(idGrupo):Observable<any>{

    
    var body = new HttpParams();
    var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.get<any>(
        URL + '/grupos/' + idGrupo + '/miembros',
      {
        headers, observe: 'response', params: body
      }
    ).pipe(map(res => {
        return res;
      }
      ));
    

  }


}
