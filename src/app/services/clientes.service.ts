// Author luisa.sanchez
// Version 1.0.0

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { Cliente } from '../models/client';

// url comun
const URL = '/podemos';

@Injectable()
export class ClientesService {


  constructor(private readonly http: HttpClient) { }

    /** Proposito: Enviar solicitus http para recuperar total de clientes
    * @author: luisa.sanchez
    * @version 1.0.0
   */
 obtenerClientes():Observable<any>{

    var body = new HttpParams();
    var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.get<any>(
        URL + '/clientes',
      {
        headers, observe: 'response', params: body
      }
    ).pipe(map(res => {
        return res;
      }
      ));
    

  }

  /** Proposito: Enviar solicitus http para recuperar cliente por medio de id
    * @author: luisa.sanchez
    * @param idCliente identificador del cliente a buscar
    * @version 1.0.0
   */
  obtenerClientePorId(idCliente):Observable<any>{

    
    var body = new HttpParams();
    var headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.get<any>(
        URL + '/clientes/' + idCliente,
      {
        headers, observe: 'response', params: body
      }
    ).pipe(map(res => {
        return res;
      }
      ));
    

  }

  /**
      Proposito: Enviar solicitud http para guardar cliente
      * @author: luisa.sanchez
      * @param cliente - objeto con informacion del cliente a enviar 
      * @version 1.0.0 
    */
 guardarCliente(cliente: Cliente ) {

  let data = {
     id: cliente.id ? cliente.id : '',
      nombre: cliente.nombre ? cliente.nombre : ''
   };

  const headers = new HttpHeaders({ 'Content-Type': 'application/json'});

  return this.http.post(
    URL + '/clientes/new', data,
    { headers, observe: 'response' }).pipe(map(res => {
      return res;
    }));
}

}
