import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// importaciones

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';

// componentes

import { AppComponent } from './app.component';

// importacion de enrutamiento
import { AppRoutingModule } from './app-routing.module';

// importacion de modulos

import { ViewsModule } from './views/views.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ViewsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
