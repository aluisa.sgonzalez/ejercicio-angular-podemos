import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsUpdateCreateComponent } from './views/clients/clients-update-create/clients-update-create.component';
import { ClientsComponent } from './views/clients/clients.component';
import { GroupMembersComponent } from './views/groups/group-members/group-members.component';
import { GroupViewComponent } from './views/groups/group-view/group-view.component';
import { GroupsComponent } from './views/groups/groups.component';


const routes: Routes = [
  {
    path: 'clients',
    component: ClientsComponent
  },
  {
    path: 'clients/new',
    component: ClientsUpdateCreateComponent
  },
  {
    path: 'clients/update/:id',
    component: ClientsUpdateCreateComponent
  },
  {
    path: 'groups',
    component: GroupsComponent
  },
  {
    path: 'groups/:id',
    component: GroupViewComponent
  },
  {
    path: 'groups/:id/members',
    component: GroupMembersComponent
  },
  {
  path: '**', redirectTo: 'clients', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
