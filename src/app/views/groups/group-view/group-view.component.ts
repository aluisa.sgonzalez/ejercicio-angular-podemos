import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GruposService } from './../../../services/groups.service';
import { Grupo } from '../../../models/groups';

@Component({
  selector: 'app-group-view',
  templateUrl: './group-view.component.html',
  styleUrls: ['./group-view.component.scss'],
  providers:[
    GruposService
  ]
})
export class GroupViewComponent implements OnInit, AfterViewInit {

   // variables de interaccion con peticin para recuperar clientes
   id = null;

   grupoRecuperado: Grupo = null;
 
   // variables para interactuar con formulario
   public grupoForm: FormGroup;
   
   constructor(
     private readonly router: Router,
     private readonly activatedRoute: ActivatedRoute,
     private readonly formBuilder: FormBuilder,
     private readonly service: GruposService
   ) {
     this.grupoForm = formBuilder.group({
       campoNombre: [null],
       campoId: [null]
     });
   }
 
   ngOnInit(): void {
     this.recuperarIdGrupo();
   }
 
   ngAfterViewInit(): void {
     // se inicia formulario solo cuando se tiene un id para realizar la peticion
     if (this.id != null && this.router.url === (`/groups/${this.id}`) ) {
       this.InicializarFormularioGrupo();
     }
   }
 
     /** Proposito: Recuperar el id de grupo de la ruta
     * @author: luisa.sanchez
     * @version 1.0.0
    */
   recuperarIdGrupo() {
     this.id = this.activatedRoute.snapshot.params.id;
   }
 
     /** Proposito: Llamar al servicio que recupera los datos del grupo solicitado e iniciar la funcion para llenar formulario
     * @author: luisa.sanchez
     * @version 1.0.0
    */
   InicializarFormularioGrupo() {
   
     const idGrupo = this.id;
   
     this.service.obtenerGruposPorId( idGrupo ).subscribe(
       data => {
       this.grupoRecuperado = data.body;
       console.log(this.grupoRecuperado);
       // asignar valores al formulario solo si se recuperan datos
       if ( this.id != null && this.grupoRecuperado != null) {
         this.asignarValorFormulario();
       }
     }, error => {
     });
   }
 
   /** Proposito: Asignar al formulario los valores recuperados
     * @author: luisa.sanchez
     * @version 1.0.0
    */
   asignarValorFormulario() {
 
     this.grupoForm.get('campoId').patchValue(this.grupoRecuperado.id);
     this.grupoForm.get('campoNombre').patchValue(this.grupoRecuperado.nombre);
   }

   verMiembros(){
    this.router.navigate(['/groups/' + this.id + '/members']);
   }

 }
 