/**
 * Proposito: crear la funcionalidad para crear interacciones con la vista del componente
 */

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator} from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';

import { merge, of } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { Grupo } from '../../../models/groups';
import { GruposService } from './../../../services/groups.service';

@Component({
  selector: 'app-group-members',
  templateUrl: './group-members.component.html',
  styleUrls: ['./group-members.component.scss'],
  providers:[
    GruposService
  ]
})
export class GroupMembersComponent implements OnInit, AfterViewInit {

  // variables de interaccion con peticion para recuperar miembros
  id = null;

  // configuracion para el paginador 
  @ViewChild('tablaPadrePaginator', { static: false }) tablaPadrePaginator: MatPaginator;

  // Variables de interaccion con la tabla
  public dataSourceGrupos: Grupo[];

  refreshPage: number;
  isRefreshing = false;

  public resultsLength = 0;

  columnasTablaGrupos = [
    'idGrupo',
    'nombre',
  'detalles'];

  constructor(
    private readonly service: GruposService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.recuperarIdGrupo();
  }

  ngAfterViewInit(): void {
    // se inicia formulario solo cuando se tiene un id para realizar la peticion
    if (this.id != null && this.router.url === (`/groups/${this.id}/members`) ) {
      this.realizarBusquedaMiembros();
    }
    
  }

  /** Proposito: Recuperar el id de grupo de la ruta
     * @author: luisa.sanchez
     * @version 1.0.0
    */
   recuperarIdGrupo() {
    this.id = this.activatedRoute.snapshot.params.id;
  }

    /** Proposito: realizar la peticion al servicio para recuperar el total de grupos
      * @author: luisa.sanchez
      * @version 1.0.0
     */
  realizarBusquedaMiembros() {
    merge( this.tablaPadrePaginator.page)
    .pipe(
      startWith({}),
      switchMap(() => {
          this.refreshPage = this.tablaPadrePaginator.pageIndex + 1;
  
          return this.service.obtenerMiembrosPorIdGrupo(this.id);
      }),
      map(
        res => {
          this.resultsLength = res.body.length;
          return res;
          
        }),
      catchError((error) => {
        return of([]);
      })
    ).subscribe(
      (data) => {
        this.dataSourceGrupos = data.body;
        console.log(data);
      },
      error => {
      }
    );
  }

}

