/**
 * Proposito: crear la funcionalidad para crear interacciones con la vista del componente
 */

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator} from '@angular/material/paginator';

import { merge, of } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { Grupo } from '../../models/groups';
import { GruposService } from './../../services/groups.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss'],
  providers:[
    GruposService
  ]
})
export class GroupsComponent implements OnInit, AfterViewInit {

  // configuracion para el paginador 
  @ViewChild('tablaPadrePaginator', { static: false }) tablaPadrePaginator: MatPaginator;

  // Variables de interaccion con la tabla
  public dataSourceGrupos: Grupo[];

  refreshPage: number;
  isRefreshing = false;

  public resultsLength = 0;

  columnasTablaGrupos = [
    'idGrupo',
    'nombre',
  'detalles'];

  constructor(
    private readonly service: GruposService,
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.realizarBusquedaGrupos();
  }

    /** Proposito: realizar la peticion al servicio para recuperar el total de grupos
      * @author: luisa.sanchez
      * @version 1.0.0
     */
  realizarBusquedaGrupos() {
    merge( this.tablaPadrePaginator.page)
    .pipe(
      startWith({}),
      switchMap(() => {
          this.refreshPage = this.tablaPadrePaginator.pageIndex + 1;
  
          return this.service.obtenerGrupos();
      }),
      map(
        res => {
          this.resultsLength = res.body.length;
          return res;
          
        }),
      catchError((error) => {
        return of([]);
      })
    ).subscribe(
      (data) => {
        this.dataSourceGrupos = data.body;
        console.log(data);
      },
      error => {
      }
    );
  }

}
