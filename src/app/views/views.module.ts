import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

import { RouterModule } from '@angular/router';

// Componentes
import { ClientsComponent } from './clients/clients.component';
import { ClientsUpdateCreateComponent } from './clients/clients-update-create/clients-update-create.component';
import { GroupsComponent } from './groups/groups.component';
import { GroupViewComponent } from './groups/group-view/group-view.component';
import { GroupMembersComponent } from './groups/group-members/group-members.component';



@NgModule({
  declarations: [ClientsComponent, ClientsUpdateCreateComponent, GroupsComponent, GroupViewComponent, GroupMembersComponent],
  exports:[
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ViewsModule { }
