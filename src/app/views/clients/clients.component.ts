/**
 * Proposito: crear la funcionalidad para crear interacciones con la vista del componente
 */

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator} from '@angular/material/paginator';

import { merge, of } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { ClientesService } from '../../services/clientes.service';
import { Cliente } from '../../models/client';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
  providers:[
    ClientesService
  ]
})
export class ClientsComponent implements OnInit, AfterViewInit {

  // configuracion para el paginador 
  @ViewChild('tablaPadrePaginator', { static: false }) tablaPadrePaginator: MatPaginator;

  // Variables de interaccion con la tabla
  public dataSourceClientes: Cliente[];

  refreshPage: number;
  isRefreshing = false;

  public resultsLength = 0;

  columnasTablaClientes = [
    'idCliente',
    'nombre',
  'detalles'];

  constructor(
    private readonly service: ClientesService,
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.realizarBusquedaClientes();
  }

    /** Proposito: realizar la peticion al servicio para recuperar el total de clientes
      * @author: luisa.sanchez
      * @version 1.0.0
     */
  realizarBusquedaClientes() {
    merge( this.tablaPadrePaginator.page)
    .pipe(
      startWith({}),
      switchMap(() => {
          this.refreshPage = this.tablaPadrePaginator.pageIndex + 1;
  
          return this.service.obtenerClientes();
      }),
      map(
        res => {
          this.resultsLength = res.body.length;
          return res;
          
        }),
      catchError((error) => {
        return of([]);
      })
    ).subscribe(
      (data) => {
        this.dataSourceClientes = data.body;
        console.log(data);
      },
      error => {
      }
    );
  }

}
