import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsUpdateCreateComponent } from './clients-update-create.component';

describe('ClientsUpdateCreateComponent', () => {
  let component: ClientsUpdateCreateComponent;
  let fixture: ComponentFixture<ClientsUpdateCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsUpdateCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsUpdateCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
