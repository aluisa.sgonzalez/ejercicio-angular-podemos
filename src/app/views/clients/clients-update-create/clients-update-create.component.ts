import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientesService } from './../../../services/clientes.service';
import { Cliente } from '../../../models/client';

@Component({
  selector: 'app-clients-update-create',
  templateUrl: './clients-update-create.component.html',
  styleUrls: ['./clients-update-create.component.scss'],
  providers:[
    ClientesService
  ]
})
export class ClientsUpdateCreateComponent implements OnInit, AfterViewInit  {

  // variables de interaccion con peticin para recuperar clientes
  id = null;

  clienteRecuperado: Cliente = null;

  // variables para interactuar con formulario
  public clienteForm: FormGroup;
  
  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly service: ClientesService
  ) {
    this.clienteForm = formBuilder.group({
      campoNombre: [null],
      campoId: [null]
    });
  }

  ngOnInit(): void {
    this.recuperarIdCliente();
  }

  ngAfterViewInit(): void {
    // se inicia formulario solo cuando se tiene un id para realizar la peticion
    if (this.id != null && this.router.url === (`/clients/update/${this.id}`) ) {
      this.InicializarFormularioContacto();
    }
  }

    /** Proposito: Recuperar el id de cliente de la ruta
    * @author: luisa.sanchez
    * @version 1.0.0
   */
  recuperarIdCliente() {
    this.id = this.activatedRoute.snapshot.params.id;
  }

    /** Proposito: Llamar al servicio que recupera los datos del cliente solicitado e iniciar la funcion para llenar formulario
    * @author: luisa.sanchez
    * @version 1.0.0
   */
  InicializarFormularioContacto() {
  
    const idCliente = this.id;
  
    this.service.obtenerClientePorId( idCliente ).subscribe(
      data => {
      this.clienteRecuperado = data.body;
      console.log(this.clienteRecuperado);
      // asignar valores al formulario solo si se recuperan datos
      if ( this.id != null && this.clienteRecuperado != null) {
        this.asignarValorFormulario();
      }
    }, error => {
    });
  }

  /** Proposito: Asignar al formulario los valores recuperados
    * @author: luisa.sanchez
    * @version 1.0.0
   */
  asignarValorFormulario() {

    this.clienteForm.get('campoId').patchValue(this.clienteRecuperado.id);
    this.clienteForm.get('campoNombre').patchValue(this.clienteRecuperado.nombre);
  }

  /** Proposito: Guardar o actualizar un cliente con los valores ingresados en el formulario
    * @author: luisa.sanchez
    * @version 1.0.0
   */
  guardarCliente() {

    const cliente = new Cliente();

    cliente.id = this.clienteForm.controls.campoId.value;
    cliente.nombre = this.clienteForm.controls.campoNombre.value;

    console.log(cliente);

    // llamara al servicio que guarda clientes
    return this.service.guardarCliente(cliente).subscribe(
      res => {
        // si la accion es correcta carga el modulo de clientes
        this.router.navigate(['/clients']);
      },
      error => {
      });
    
  }

}
