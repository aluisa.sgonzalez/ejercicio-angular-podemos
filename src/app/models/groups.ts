// Author luisa sanchez
// Version 1.0.0 

// clase con elementos del grupo
export class Grupo {
    id: string;
    nombre: string;
}

// clase con elementos del miembro
export class Miembro {
    id: string;
    grupo: string;
    estatus: string;
    monto: string;
    saldo: string;
}